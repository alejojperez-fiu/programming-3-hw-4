/*=============================================================================
|   Source code:  TestPoker.c
|        Author:  Alejandro Perez
|    Student ID:  6088462
|    Assignment:  Program #4 Assignment 4
|*===========================================================================*/

/*=============================================================================
| INCLUDES +===================================================================
|*===========================================================================*/
#include "CommonFunctionalities.h"

/*=============================================================================
| FUNCTIONS ===================================================================
|*===========================================================================*/

/**
 * Helper function to obtain hand of cards cosidered as Flush in Poker
 * 
 * Cards:
 * [ 2-C ] [ 3-C ] [ 4-C ] [ 6-C ] [ Q-C ]
 * 
 * Integer Representation:
 * [ 1-2 ] [ 1-2 ] [ 3-2 ] [ 5-2 ] [ 11-2 ]
 */
struct Hand GetTestHandFlush()
{
    struct Hand hand;

    hand.Cards[0].Rank = 1;
    hand.Cards[0].Suit = 2;

    hand.Cards[1].Rank = 1;
    hand.Cards[1].Suit = 2;

    hand.Cards[2].Rank = 3;
    hand.Cards[2].Suit = 2;

    hand.Cards[3].Rank = 5;
    hand.Cards[3].Suit = 2;

    hand.Cards[4].Rank = 11;
    hand.Cards[4].Suit = 2;

    return hand;
}

/**
 * Helper function to obtain hand of cards cosidered as Four of a Kind in Poker
 * 
 * Cards:
 * [ 3-D ] [ 3-H ] [ 3-S ] [ 3-C ] [ Q-C ]
 * 
 * Integer Representation:
 * [ 2-3 ] [ 2-1 ] [ 2-0 ] [ 2-2 ] [ 11-2 ]
 */
struct Hand GetTestHandFourOfAKind()
{
    struct Hand hand;

    hand.Cards[0].Rank = 2;
    hand.Cards[0].Suit = 3;

    hand.Cards[1].Rank = 2;
    hand.Cards[1].Suit = 1;

    hand.Cards[2].Rank = 2;
    hand.Cards[2].Suit = 0;

    hand.Cards[3].Rank = 2;
    hand.Cards[3].Suit = 2;

    hand.Cards[4].Rank = 11;
    hand.Cards[4].Suit = 2;

    return hand;
}

/**
 * Helper function to obtain hand of cards cosidered as Full House in Poker
 * 
 * Cards:
 * [ 3-D ] [ 3-H ] [ 3-S ] [ T-D ] [ T-C ]
 * 
 * Integer Representation:
 * [ 2-3 ] [ 2-1 ] [ 2-0 ] [ 9-3 ] [ 9-2 ]
 */
struct Hand GetTestHandFullHouse()
{
    struct Hand hand;

    hand.Cards[0].Rank = 2;
    hand.Cards[0].Suit = 3;

    hand.Cards[1].Rank = 2;
    hand.Cards[1].Suit = 1;

    hand.Cards[2].Rank = 2;
    hand.Cards[2].Suit = 0;

    hand.Cards[3].Rank = 9;
    hand.Cards[3].Suit = 3;

    hand.Cards[4].Rank = 9;
    hand.Cards[4].Suit = 2;

    return hand;
}

/**
 * Helper function to obtain hand of cards cosidered as High Card in Poker
 * 
 * Cards:
 * [ 2-D ] [ 3-C ] [ 4-D ] [ 6-S ] [ Q-H ]
 * 
 * Integer Representation:
 * [ 1-3 ] [ 2-2 ] [ 3-3 ] [ 5-0 ] [ 11-1 ]
 */
struct Hand GetTestHandHighCard()
{
    struct Hand hand;

    hand.Cards[0].Rank = 1;
    hand.Cards[0].Suit = 3;

    hand.Cards[1].Rank = 2;
    hand.Cards[1].Suit = 2;

    hand.Cards[2].Rank = 3;
    hand.Cards[2].Suit = 3;

    hand.Cards[3].Rank = 5;
    hand.Cards[3].Suit = 0;

    hand.Cards[4].Rank = 11;
    hand.Cards[4].Suit = 1;

    return hand;
}

/**
 * Helper function to obtain hand of cards cosidered as One Pair in Poker
 * 
 * Cards:
 * [ 4-H ] [ 5-H ] [ 5-D ] [ 7-H ] [ T-S ]
 * 
 * Integer Representation:
 * [ 3-1 ] [ 4-1 ] [ 4-3 ] [ 6-1 ] [ 9-0 ]
 */
struct Hand GetTestHandOnePair()
{
    struct Hand hand;

    hand.Cards[0].Rank = 3;
    hand.Cards[0].Suit = 1;

    hand.Cards[1].Rank = 4;
    hand.Cards[1].Suit = 1;

    hand.Cards[2].Rank = 4;
    hand.Cards[2].Suit = 3;

    hand.Cards[3].Rank = 6;
    hand.Cards[3].Suit = 1;

    hand.Cards[4].Rank = 9;
    hand.Cards[4].Suit = 0;

    return hand;
}

/**
 * Helper function to obtain hand of cards cosidered as Straight in Poker
 * 
 * Cards:
 * [ A-H ] [ 2-D ] [ 3-C ] [ 4-D ] [ 5-D ]
 * 
 * Integer Representation:
 * [ 0-1 ] [ 1-3 ] [ 2-2 ] [ 3-3 ] [ 4-3 ]
 */
struct Hand GetTestHandStraight()
{
    struct Hand hand;

    hand.Cards[0].Rank = 0;
    hand.Cards[0].Suit = 1;

    hand.Cards[1].Rank = 1;
    hand.Cards[1].Suit = 3;

    hand.Cards[2].Rank = 2;
    hand.Cards[2].Suit = 2;

    hand.Cards[3].Rank = 3;
    hand.Cards[3].Suit = 3;

    hand.Cards[4].Rank = 4;
    hand.Cards[4].Suit = 3;

    return hand;
}

/**
 * Helper function to obtain hand of cards cosidered as Straight Flush in Poker
 * 
 * Cards:
 * [ A-D ] [ T-D ] [ J-D ] [ Q-D ] [ K-D ]
 * 
 * Integer Representation:
 * [ 0-3 ] [ 9-3 ] [ 10-3 ] [ 11-3 ] [ 12-3 ]
 */
struct Hand GetTestHandStraightFlush()
{
    struct Hand hand;

    hand.Cards[0].Rank = 0;
    hand.Cards[0].Suit = 3;

    hand.Cards[1].Rank = 9;
    hand.Cards[1].Suit = 3;

    hand.Cards[2].Rank = 10;
    hand.Cards[2].Suit = 3;

    hand.Cards[3].Rank = 11;
    hand.Cards[3].Suit = 3;

    hand.Cards[4].Rank = 12;
    hand.Cards[4].Suit = 3;

    return hand;
}

/**
 * Helper function to obtain hand of cards cosidered as Three of a Kind in Poker
 * 
 * Cards:
 * [ 3-D ] [ 3-H ] [ 3-S ] [ T-D ] [ Q-C ]
 * 
 * Integer Representation:
 * [ 2-3 ] [ 2-1 ] [ 2-0 ] [ 9-3 ] [ 11-2 ]
 */
struct Hand GetTestHandThreeOfAKind()
{
    struct Hand hand;

    hand.Cards[0].Rank = 2;
    hand.Cards[0].Suit = 3;

    hand.Cards[1].Rank = 2;
    hand.Cards[1].Suit = 1;

    hand.Cards[2].Rank = 2;
    hand.Cards[2].Suit = 0;

    hand.Cards[3].Rank = 9;
    hand.Cards[3].Suit = 3;

    hand.Cards[4].Rank = 11;
    hand.Cards[4].Suit = 2;

    return hand;
}

/**
 * Helper function to obtain hand of cards cosidered as Two Pairs in Poker
 * 
 * Cards:
 * [ 3-D ] [ 3-H ] [ T-C ] [ T-D ] [ Q-C ]
 * 
 * Integer Representation:
 * [ 2-3 ] [ 2-1 ] [ 9-2 ] [ 9-3 ] [ 11-2 ]
 */
struct Hand GetTestHandTwoPairs()
{
    struct Hand hand;

    hand.Cards[0].Rank = 2;
    hand.Cards[0].Suit = 3;

    hand.Cards[1].Rank = 2;
    hand.Cards[1].Suit = 1;

    hand.Cards[2].Rank = 9;
    hand.Cards[2].Suit = 2;

    hand.Cards[3].Rank = 9;
    hand.Cards[3].Suit = 3;

    hand.Cards[4].Rank = 11;
    hand.Cards[4].Suit = 2;

    return hand;
}

/**
 * Helper function that builds a group of testing hands and prints them
 * 
 * Testing Hands Count: 9
 * 1. High Card
 * 2. One Pair
 * 3. Two Pairs
 * 4. Three of a Kind
 * 5. Straight
 * 6. Flush
 * 7. Full House
 * 8. Four of a Kind
 * 9. Straight Flush
 */
void PrintTestHands()
{
    int amountOfTestHands = 9;

    struct Hand playersHands[amountOfTestHands];

    playersHands[0] = GetTestHandHighCard();
    playersHands[1] = GetTestHandOnePair();
    playersHands[2] = GetTestHandTwoPairs();
    playersHands[3] = GetTestHandThreeOfAKind();
    playersHands[4] = GetTestHandStraight();
    playersHands[5] = GetTestHandFlush();
    playersHands[6] = GetTestHandFullHouse();
    playersHands[7] = GetTestHandFourOfAKind();
    playersHands[8] = GetTestHandStraightFlush();

    RankPlayersHands(playersHands, amountOfTestHands);

    PrintTextSuccess("Player Hands: test\n");
    PrintPlayersDeck(playersHands, amountOfTestHands, AMOUNT_OF_CARDS_PER_PLAYER, true, false);
}