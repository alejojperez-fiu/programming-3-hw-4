/*=============================================================================
|   Source code:  PokerGameMain.c
|        Author:  Alejandro Perez
|    Student ID:  6088462
|    Assignment:  Program #4 Assignment 
|
|        Course:  COP 4338
|       Section:  U03
|    Instructor:  William Feild
|      Due Date:  October 23, 2018, at the beginning of class
|
|	I hereby certify that this collective work is my own
|	and none of it is the work of any other person or entity.
|
|	___Alejandro Perez_________________________ [Signature]
|
|      Language:  C
|   Compile/Run:  Compile PokerGame.c and CardsDeckOperations.c. 
|                 Make sure that the header file CommonFunctionalities.h
|                 is in the same path where your are compiling
|                 the source code.
|
|   Compile:
| 	Option 1: make
|   Option 2: make build
|   Option 3: gcc PokerGameMain.c CardsDeckOperations.c RankPokerHand.c TestPoker.c -w -o PokerGame.out
|           // -w This tag is used to hide any warning at compile time
|
|   Run:
|   ./PokerGame.out 5 5
|       // First argument is the amount of cards: integer [MIN_INPUT_VALUE - MAX_INPUT_VALUE]
|       // Second argument is the amount of players: integer [MIN_INPUT_VALUE - MAX_INPUT_VALUE]
| +-----------------------------------------------------------------------------
|
| Description: Based on the console input [argument 1: cards per player
|              (ignored: always set to 5), argument 2: amount of players] this
|              application creates a new deck of cards and prints its content,
|              then it is shuffled. The program iterates through each user and
|              from the shuffled deck, it deals the cards. At the end, it
|              prints the original deck, the shuffled deck, each player's hand,
|              each player's hand sorted, each players hand ranked, all the
|              players' hand marking the winner hand, and a test collection of
|              hands including any poker hand possibility.
|
|       Input: Console Arguments
|               - cards per player [ignored: always set to 5]
|               - amount of players
|
|      Output: The original deck, the shuffled deck, each player's hand,
|              each player's hand sorted, each players hand ranked, all the
|              players' hand marking the winner hand, and a test collection of
|              hands including any poker hand possibility.
|
|     Process: 
|            - Shuffle Algorithm: Fisher–Yates shuffle
|              Reference: https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm
|              For shuffling the deck, we are iterating through each card in the
|              deck, and in each iteration we are finding another card in the
|              deck and swaping its position with the current iteration's card.
|              By doing it this way we are guaranteeing that each card has the
|              ability to be shuffled, but also we are making sure the each card
|              can be swapped to its original position. Therefore, we can
|              consider that the algorithm is for shuffling is random.
|            - For sorting each player's hand we are using insertion sort. For
|              reference visit: https://www.geeksforgeeks.org/insertion-sort/
|
|   Required Features Not Included: N/A
|
|   Known Bugs:  N/A
|*===========================================================================*/

/**
 * "CommonFunctionalities.h" It is being used to group all common functionality
 *                           among the source code files
 */
#include "CommonFunctionalities.h"

/**
 * Program entry point
 *
 * @return Program execution status. If different
 * from SUCCESS then something was not correct.
 */
int main(int argc, char **args)
{
    PrintProgramHeader();
    
    int numberOfCards;
    int numberOfPlayers;

    bool validInput = ValidateConsoleValues(
        argc,
        args,
        &numberOfCards,
        &numberOfPlayers
    );

    if(validInput == false)
    {
        PrintTextWarning(
            "Gracefully shutting down the program"
            " due to invalid input...\n"
        );

        return ERROR;
    }

    srand(time(NULL));

    PokerGame(numberOfCards, numberOfPlayers);

    return SUCCESS;
}

/**
 * Print a description of what the program does to
 * the end user
 */
void PrintProgramHeader()
{
    printf("\nSIMULATE POKER GAME\n\n");

    printf("----------------------------------------------\n"
           "| This Application accepts two console       |\n"
           "| arguments. First argument (integer in the  |\n"
           "| range [%2d-%2d]) is the amount of cards      |\n"
           "| each player should have, which in this     |\n"
           "| case is ignored because for Poker, each    |\n"
           "| player should have %1d cards), second       |\n"
           "| argument (integer in the range [%2d-%2d])    |\n"
           "| is for the amount of players.              |\n"
           "|                                            |\n"
           "| The program creates a new deck of cards,   |\n"
           "| shuffles it randomly, and then it deals    |\n"
           "| the amount of cards specified to each      |\n"
           "| player. Then it prints the original deck,  |\n"
           "| the shuffled deck, each player's hand,     |\n"
           "| each player's hand sorted, each players    |\n"
           "| hand ranked, all the players' hand marking |\n"
           "| the winner hand, and a test collection of  |\n"
           "| hands including any poker hand possibility |\n"
           "----------------------------------------------\n\n",
           MIN_INPUT_VALUE,
           MAX_INPUT_VALUE,
           AMOUNT_OF_CARDS_PER_PLAYER,
           MIN_INPUT_VALUE,
           MAX_INPUT_VALUE
           );
}

/**
 * Helper function to print text to the console using the constant 
 * CONSOLE_COLOR_RED as the font color.
 * 
 * @param text The text that is going to be printed
 */
void PrintTextDanger(char text[])
{
    PrintTextWithColor(text, CONSOLE_COLOR_RED);
}

/**
 * Helper function to print text to the console using the constant 
 * CONSOLE_COLOR_GREEN as the font color.
 * 
 * @param text The text that is going to be printed
 */
void PrintTextSuccess(char text[])
{
    PrintTextWithColor(text, CONSOLE_COLOR_GREEN);
}

/**
 * Helper function to print text to the console using the constant 
 * CONSOLE_COLOR_YELLOW as the font color.
 * 
 * @param text The text that is going to be printed
 */
void PrintTextWarning(char text[])
{
    PrintTextWithColor(text, CONSOLE_COLOR_YELLOW);
}

/**
 * Helper function to print text to the console using a color.
 * 
 * @param text The text that is going to be printed
 * @param color The color to be used when printing the text
 */
void PrintTextWithColor(char text[], char color[])
{
    printf("%s", color);
    printf("%s", text);
    printf(CONSOLE_COLOR_RESET);
}

/**
 * Validate that the arguments count provided is the correct one for the program
 * to work.
 * 
 * @param count The console's arguments count provided by the user
 * 
 * @return Whether the count parameter is valid or not 
 */
bool ValidateConsoleArgumentsCount(int count)
{
    if(count < MIN_INPUT_VALID_ARGUMENTS_COUNT)
    {
        PrintTextDanger("The amount of arguments provided is not valid.\n\n");
        return false;
    }

    return true;
}

/**
 * Validate the console input from the user against the constant boundaries
 * values, and make sure that there is enough cards in the deck for the amount
 * of users and the amount of cards per user specified. This function also
 * prints the specific error that the input has in case there is such error.
 * Since we are playing poker, it overrides the amount of cards per player to be
 * the correct one.
 * 
 * @param argc The amount of arguments that are in the args parameter
 * @param args The array containing the console input
 * 
 * @return Whether the input is valid or not
 */
bool ValidateConsoleValues(
    int argc, 
    char **args, 
    int *cardsPerPlayer, 
    int *players
)
{
    bool inputArgumentsCountIsValid = ValidateConsoleArgumentsCount(argc);
    if(inputArgumentsCountIsValid == false)
    {
        return false;
    }

    char *ignorePtr = NULL;

    *cardsPerPlayer = (int) strtol(
        args[INPUT_AMOUNT_OF_CARDS_POSITION],
        &ignorePtr,
        BASE_TEN
    );

    *players = (int) strtol(
        args[INPUT_AMOUNT_OF_PLAYERS_POSITION],
        &ignorePtr,
        BASE_TEN
    );

    if(
        ValidateAmountOfCards(*cardsPerPlayer) == false
        ||
        ValidateAmountOfPlayers(*players) == false
        ||
        ValidateEnoughCardsToPlay(*cardsPerPlayer, *players) == false
    )
    {
        return false;
    }

    *cardsPerPlayer = AMOUNT_OF_CARDS_PER_PLAYER;

    return true;
}