/*=============================================================================
|   Source code:  CardsDeckOperations.c
|        Author:  Alejandro Perez
|    Student ID:  6088462
|    Assignment:  Program #4 Assignment 4
|*===========================================================================*/

/*=============================================================================
| INCLUDES +===================================================================
|*===========================================================================*/
#include "CommonFunctionalities.h"

/*=============================================================================
| FUNCTIONS ===================================================================
|*===========================================================================*/

/**
 * Helper function that calculates the amount of card required to play based on
 * the amount of players and the amount of cards per player. The formula is:
 * 
 * numberOfPlayers * numberOfCards
 * 
 * @return The amount of cards required based of players and cards per player
 */
int AmountOfCardRequiredForPlayers(int numberOfPlayers, int numberOfCards)
{
    return numberOfPlayers * numberOfCards;
}

/**
 * Helper function to fill out the players deck based on the amount of players,
 * the amount of cards per player, and a valid deck of cards.
 * 
 * @param deck The deck from where the cards are going to be dealt
 * @param playersDeck The deck where the cards of each player are going to be
 *                    stored
 * @param players The amount of players
 * @param cardsPerPlayer The amount of cards per player
 */
void DealCardsToPlayers(
    const struct Card deck[], 
    struct Hand playersHands[], 
    int players, 
    int cardsPerPlayer
)
{
    int playersCounter = 0;
    int deckCurrentPosition = 0;

    for(playersCounter = 0; playersCounter < players; playersCounter++)
    {
        int cardsCounter = 0;

        for(cardsCounter = 0; cardsCounter < cardsPerPlayer; cardsCounter++)
        {
            playersHands[playersCounter].Cards[cardsCounter] =
                deck[deckCurrentPosition];

            deckCurrentPosition++;
        }
    }
}

/**
 * Helper function to find the amount of cards in a deck. It multiplies the
 * amount of cards per suit times the amount of suits.
 * 
 * @retrun The amount of cards in a deck
 */
int GetAmountOfCards()
{
    static const int cardsInDeck = CARD_SUITS * CARD_RANKS;

    return cardsInDeck;
}

/**
 * Helper function to obtain a card's rank character from its integer equivalent
 * 
 * @param cardRank The integer card rank value
 * 
 * @return The equivalent value of the integer rank as a char
 */
char GetCardRankCharacter(int cardRank)
{
    static const char ranks[] = {
        'A', '2', '3', '4', '5', '6',
        '7', '8', '9', 'T', 'J', 'Q', 'K'
    };

    return ranks[cardRank];
}

/**
 * Helper function to obtain a hand's name from its integer equivalent
 * 
 * @param handValue The integer hand's name value
 * 
 * @return The equivalent value of the integer hand name as a string
 */
char* GetHandName(int handValue)
{
    static const char *handNames[] = {
        "High Card",
        "One Pair",
        "Two Pairs",
        "Three of a Kind",
        "Straight",
        "Flush",
        "Full House",
        "Four of a Kind",
        "Straight Flush"
    };

    return handNames[handValue];
}

/**
 * Helper function to obtain a card's suit character from its integer equivalent
 * 
 * @param cardSuit The integer card suit value
 * 
 * @return The equivalent value of the integer rank as a char
 */
char GetCardSuitCharacter(int cardSuit)
{
    static const char suits[] = {
        'S', 'H', 'C', 'D'
    };

    return suits[cardSuit];
}

/**
 * Get a random index position in a deck of cards
 */
int GetRandomPositionInDeck()
{
    return rand() % GetAmountOfCards();
}

/**
 * Helper function to obtain the rank of a card based on the initial position
 * in the deck array
 * 
 * @param positionInDeck The position the we want the rank of
 * 
 * @return The rank integer representation based on the position
 */
int InitializeCardRank(int positionInDeck)
{
    return positionInDeck % CARD_RANKS;
}

/**
 * Helper function to obtain the suit of a card based on the initial position
 * in the deck array
 * 
 * @param positionInDeck The position the we want the suit of
 * 
 * @return The suit integer representation based on the position
 */
int InitializeCardSuit(int positionInDeck)
{
    return positionInDeck / CARD_RANKS;
}

/**
 * Helper function to fill the deck content with cards in an organized manner
 * 
 * @param deck The deck that is going to be filled
 */
void InitializeDeck(struct Card deck[])
{
    int cardPositionInDeck = 0;

    for(
        cardPositionInDeck = 0;
        cardPositionInDeck < GetAmountOfCards();
        cardPositionInDeck++
    )
    {        
        deck[cardPositionInDeck].Rank = InitializeCardRank(cardPositionInDeck);
        deck[cardPositionInDeck].Suit = InitializeCardSuit(cardPositionInDeck);
    }
}

/**
 * Helper function to order the cards within a single player hand
 * 
 * Sorting Algorithm: Insertion Sort
 * Reference: https://www.geeksforgeeks.org/insertion-sort/
 * 
 * @property playerHand The the hand of cards that is going to be sorted
 */
void OrderPlayerHand(struct Hand *playerHand)
{
    int currentIteration = 0;
    struct Card currentCard;
    int previousIteration = 0;

    for (
        currentIteration = 1;
        currentIteration < AMOUNT_OF_CARDS_PER_PLAYER;
        currentIteration++
    ) 
    { 
        currentCard = playerHand->Cards[currentIteration]; 
        previousIteration = currentIteration - 1; 
  
        while (previousIteration >= 0 && playerHand->Cards[previousIteration].Rank > currentCard.Rank) 
        { 
            playerHand->Cards[previousIteration + 1] = playerHand->Cards[previousIteration]; 
            previousIteration = previousIteration - 1; 
        } 

        playerHand->Cards[previousIteration + 1] = currentCard;
   }
}

/**
 * This function accepts the amount of cards per player and the amount of
 * players. It first prints to the user what is the input that it will work
 * with, and the amount of cards in the deck. Then it creates a new deck of
 * cards and prints its content to the end user. Later, that deck is shuffled
 * and printed again, so the user sees the final state of the deck. Then, it
 * creates a hand for each user and deals the cards from the shuffled deck. Once
 * it finishes, it prints the cards of each player. It sorts each player's hand
 * and prints them. It categorizes all the hands by their poker's name and
 * prints them again. It determines who are the winners and print the players'
 * hand marking who the winner is. Finally it prints a collection of hands for
 * testing purposes.
 * 
 * @param cardsPerPlayer The amount of cards per player
 * @param players The amount of players
 */
void PokerGame(const int cardsPerPlayer, const int players)
{
    PrintInput(cardsPerPlayer, players);

    int amountOfCardsInDeck = GetAmountOfCards();
    struct Card deck[amountOfCardsInDeck];

    PrintTextSuccess("Deck Size: ");
    printf("%d\n\n", amountOfCardsInDeck);

    InitializeDeck(deck);
    PrintTextSuccess("Original Ordered Deck:\n");
    PrintDeck(deck);

    puts("");

    ShuffleDeck(deck);
    PrintTextSuccess("Random Shuffled Deck:\n");
    PrintDeck(deck);

    puts("");

    struct Hand playersHands[players];
    
    DealCardsToPlayers(deck, playersHands, players, cardsPerPlayer);
    PrintTextSuccess("Player Hands: (dealt from top/front of deck):\n");
    PrintPlayersDeck(playersHands, players, cardsPerPlayer, false, false);

    puts("");

    RankPlayersHands(playersHands, players);

    PrintTextSuccess("Player Hands: sorted\n");
    PrintPlayersDeck(playersHands, players, cardsPerPlayer, false, false);

    puts("");

    PrintTextSuccess("Player Hands: ranked\n");
    PrintPlayersDeck(playersHands, players, cardsPerPlayer, true, false);

    puts("");

    PrintTextSuccess("Player Hands: winner(s)\n");
    PrintPlayersDeck(playersHands, players, cardsPerPlayer, true, true);

    puts("");

    PrintTestHands();
}

/**
 * Helper function to print the suit and rank of a card
 * 
 * @param card The card that is going to be printed
 */
void PrintCard(struct Card card)
{
    printf("[ %c-%c ]", GetCardRankCharacter(card.Rank), GetCardSuitCharacter(card.Suit));
}

/**
 * Helper function to print the contents of a deck of cards
 * 
 * @param deck The deck of card tht is going to be printed
 */
void PrintDeck(const struct Card deck[])
{
    int cardPositionInDeck = 0;

    for(
        cardPositionInDeck = 0;
        cardPositionInDeck < GetAmountOfCards();
        cardPositionInDeck++
    )
    {
        if(cardPositionInDeck % CARD_RANKS != 0)
        {
            printf(" ");
        }

        PrintCard(deck[cardPositionInDeck]);

        if(cardPositionInDeck % CARD_RANKS == CARD_RANKS - 1)
        {
            printf("\n");
        }
    }
}

/**
 * Helper function that prints the input received to the user, so (s)he knows
 * what are the parameters being used when computing the result
 */
void PrintInput(int cardsPerPlayer, int players)
{
    PrintTextSuccess("Input:\n");
    printf("Cards per player: %d\n", cardsPerPlayer);
    printf("Number of players: %d\n\n", players);
}

/**
 * Helper function to print the content of the players' deck of cards
 * 
 * @property playersHands The list hand that are going to be printed
 * @property players The amount of hands in the list provided
 * @property cardsPerPlayer The amount of card that each player has
 * @property printPokerHandName Wether or not it should print the name of the 
 *                              hand
 * @property printWinner Wether or not it should print if the hand is a winner
 *                       hand
 */
void PrintPlayersDeck(
    const struct Hand playersHands[], 
    int players, 
    int cardsPerPlayer,
    bool printPokerHandName,
    bool printWinner
)
{
    int playersCounter = 0;

    for(playersCounter = 0; playersCounter < players; playersCounter++)
    {
        int cardsCounter = 0;
        int playerNumber = playersCounter + 1;

        printf("Player %d] - ", playerNumber);

        for(cardsCounter = 0; cardsCounter < cardsPerPlayer; cardsCounter++)
        {
            if(cardsCounter != 0)
            {
                printf(" ");
            }

            PrintCard(playersHands[playersCounter].Cards[cardsCounter]);
        }

        if(printPokerHandName == true)
        {
            printf(
                " - %s",
                GetHandName(playersHands[playersCounter].Category)
            );
        }

        if(printWinner == true && playersHands[playersCounter].Winner == true)
        {
            printf(" - %s", "Winner");
        }

        puts("");
    }
}

/**
 * Helper function to rank players hands
 * 
 * @property playersHands The list of players hands that are going to be ranked
 * @property hands The amount of hands in the list provided
 */
void RankPlayersHands(struct Hand playersHands[], int hands)
{
    int currentHand = 0;
    int higestHandCategory = 0;

    for(currentHand = 0; currentHand < hands; currentHand++)
    {
        OrderPlayerHand(&playersHands[currentHand]);
        RankPlayerHand(&playersHands[currentHand]);

        if(playersHands[currentHand].Category > higestHandCategory)
        {
            higestHandCategory = playersHands[currentHand].Category;
        }
    }

    for(currentHand = 0; currentHand < hands; currentHand++)
    {
        if(playersHands[currentHand].Category == higestHandCategory)
        {
            playersHands[currentHand].Winner = true;
        }
    }
}

/**
 * Helper function that accepts a deck of cards and shuffles it.
 * 
 * Shuffle Algorithm: Fisher–Yates shuffle
 * Reference: https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm
 * 
 * Suffle Procedure: Following the instructions discussed by William Feild, we
 * are going to iterate through the array of cards, and in each iteration we are
 * going to find a random position in the array where the value of the current
 * iteration is going to be swapped with the randomly obtained the postion. By
 * doing this we are giving the opportunity to each card to be moved from its
 * original position, but also we have the possibility that the same card could
 * be returned to its original position as well.
 * 
 * @param deck The deck of cards that is going to be shuffled
 */
void ShuffleDeck(struct Card deck[])
{
    int deckPosition = 0;

    for(deckPosition = 0; deckPosition < GetAmountOfCards(); deckPosition++)
    {
        SwapCardsPositionInDeck(deck, deckPosition, GetRandomPositionInDeck());
    }
}

/**
 * Get two positions in a deck of cards and swap the values. Create a temporal
 * value holder that is going to hold the value of the original postion, then
 * fill the original postion with the swap postion, and finally put the hold
 * value in the swap postion.
 * 
 * @param deck The deck where the cards are
 * @param deckPosition The original postion that is going to be swapped
 * @param swapPosition The position that is going to be swapped with the
 *                     original postion
 */
void SwapCardsPositionInDeck(
    struct Card deck[],
    int deckPosition,
    int swapPosition
)
{
    struct Card temporalCard;

    temporalCard = deck[deckPosition];

    deck[deckPosition] = deck[swapPosition];

    deck[swapPosition] = temporalCard;
}

/**
 * Helper function that validates the amount of cards allowed by the program
 * 
 * @return Whether the amount of cards is valid or not
 */
bool ValidateAmountOfCards(int cards)
{
    if(cards < MIN_INPUT_VALUE || cards > MAX_INPUT_VALUE)
    {
        PrintTextDanger(
            "The amount of cards per player specified is not valid.\n"
            "Refer to the program description to see the allowed values.\n\n"
        );

        return false;
    }
}

/**
 * Helper function that validates the amount of players allowed by the program
 * 
 * @return Whether the amount of players is valid or not
 */
bool ValidateAmountOfPlayers(int players)
{
    if(players < MIN_INPUT_VALUE || players > MAX_INPUT_VALUE)
    {
        PrintTextDanger(
            "The amount of cards per player specified is not valid.\n"
            "Refer to the program description to see the allowed values.\n\n"
        );

        return false;
    }
}

/**
 * Helper function that validates that there is enough cards to deal to each
 * player
 * 
 * @return Whether the amount of cards is enough to play
 */
bool ValidateEnoughCardsToPlay(int cards, int players)
{
    if(AmountOfCardRequiredForPlayers(cards, players) > GetAmountOfCards())
    {
        PrintTextDanger(
            "There is not enough cards for the amount of players\n"
            "and the amount of cards per player specified.\n\n"
        );

        return false;
    }
}