#=============================================================================
#   Source code:  Makefile
#        Author:  Alejandro Perez
#    Student ID:  6088462
#    Assignment:  Program #4 Assignment 4
#=============================================================================

# The compiler: gcc for C program
CC = gcc

# the build target executable:
TARGET = PokerGame.out

# -w This tag is used to hide any warning at compile time
build:
	$(CC) PokerGameMain.c CardsDeckOperations.c RankPokerHand.c TestPoker.c -w -o $(TARGET)

clean:
	rm $(TARGET)