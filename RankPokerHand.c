/*=============================================================================
|   Source code:  RankPokerHand.c
|        Author:  Alejandro Perez
|    Student ID:  6088462
|    Assignment:  Program #4 Assignment 4
|*===========================================================================*/

/*=============================================================================
| INCLUDES +===================================================================
|*===========================================================================*/
#include "CommonFunctionalities.h"

/*=============================================================================
| FUNCTIONS ===================================================================
|*===========================================================================*/

/**
 * Helper function to rank the hand provided based on the categories of poker
 * 
 * @property playerHand The the hand of cards that is going to be ranked
 */
void RankPlayerHand(struct Hand *playerHand)
{
    bool isFlush = IsFlush(*playerHand);
    bool isStraight = IsStraight(*playerHand);

    if(isFlush && isStraight)
    {
        playerHand->Category = POKER_HAND_STRAIGHT_FLUSH;
    }
    else if(IsFourOfAKind(*playerHand))
    {
        playerHand->Category = POKER_HAND_FOUR_OF_A_KIND;
    }
    else if(IsFullHouse(*playerHand))
    {
        playerHand->Category = POKER_HAND_FULL_HOUSE;
    }
    else if(isFlush)
    {
        playerHand->Category = POKER_HAND_FLUSH;
    }
    else if(isStraight)
    {
        playerHand->Category = POKER_HAND_STRAIGHT;
    }
    else if(IsThreeOfAKind(*playerHand))
    {
        playerHand->Category = POKER_HAND_THREE_OF_A_KIND;
    }
    else if(IsTwoPairs(*playerHand))
    {
        playerHand->Category = POKER_HAND_TWO_PAIRS;
    }
    else if(IsOnePair(*playerHand))
    {
        playerHand->Category = POKER_HAND_ONE_PAIR;
    }
    else
    {
        playerHand->Category = POKER_HAND_HIGH_CARD;
    }
}

/**
 * Helper function to identify if a hand of cards is consider a Flush in Poker
 * 
 * @property playerHand The hand of cards that is going to be checked
 * 
 * @return Whether the hand of cards is consider a Flush or not in Poker
 */
bool IsFlush(struct Hand playerHand)
{
    int previousCard = 0;
    int currentCard = 1;
    bool onlyOneSuit = true;

    while(onlyOneSuit == true && currentCard < AMOUNT_OF_CARDS_PER_PLAYER)
    {
        previousCard = currentCard - 1;

        if(playerHand.Cards[currentCard].Suit != playerHand.Cards[previousCard].Suit)
        {
            onlyOneSuit = false;
        }

        currentCard++;
    }

    return onlyOneSuit;
}

/**
 * Helper function to identify if a hand of cards is consider Four of a Kind in 
 * Poker
 * 
 * Formula:
 * Case 1 -> x x x x y
 * We are checking that the first 4 cards have the same rank
 * 
 * Case 2 -> y x x x x
 * We are checking that the last 4 cards have the same rank
 * 
 * @property playerHand The hand of cards that is going to be checked
 * 
 * @return Whether the hand of cards is consider a Four of a Kind or not in 
 *                 Poker
 */
bool IsFourOfAKind(struct Hand playerHand)
{
    if(playerHand.Cards[0].Rank == playerHand.Cards[3].Rank)
    {
        return true;
    }

    if(playerHand.Cards[1].Rank == playerHand.Cards[4].Rank)
    {
        return true;
    }

    return false;
}

/**
 * Helper function to identify if a hand of cards is consider Full House in 
 * Poker
 * 
 * Formula:
 * Case 1 -> x x y y y
 * We are checking that the first 2 cards have the same rank and that the last 3
 * cards have the same rank
 * 
 * Case 2 -> x x x y y
 * We are checking that the first 3 cards have the same rank and that the last 2
 * cards have the same rank
 * 
 * @property playerHand The hand of cards that is going to be checked
 * 
 * @return Whether the hand of cards is consider a Full House or not in Poker
 */
bool IsFullHouse(struct Hand playerHand)
{
    if(
        playerHand.Cards[0].Rank == playerHand.Cards[1].Rank &&
        playerHand.Cards[2].Rank == playerHand.Cards[4].Rank
    )
    {
        return true;
    }

    if(
        playerHand.Cards[0].Rank == playerHand.Cards[2].Rank &&
        playerHand.Cards[3].Rank == playerHand.Cards[4].Rank
    )
    {
        return true;
    }

    return false;
}

/**
 * Helper function to identify if a hand of cards is consider One Pair in Poker
 * 
 * Formula:
 * Case 1 -> x x w y z
 * We are checking that the first 2 cards have the same rank
 * 
 * Case 2 -> w x x y z
 * We are checking that the 2nd and 3rd card have the same rank
 * 
 * Case 3 -> w y x x z
 * We are checking that the 3rd and 4th card have the same rank
 * 
 * Case 4 -> w y z x x
 * We are checking that the last 2 cards have the same rank
 * 
 * @property playerHand The hand of cards that is going to be checked
 * 
 * @return Whether the hand of cards is consider One Pair or not in Poker
 */
bool IsOnePair(struct Hand playerHand)
{
    if(
        playerHand.Cards[0].Rank == playerHand.Cards[1].Rank ||
        playerHand.Cards[1].Rank == playerHand.Cards[2].Rank ||
        playerHand.Cards[2].Rank == playerHand.Cards[3].Rank ||
        playerHand.Cards[3].Rank == playerHand.Cards[4].Rank
    )
    {
        return true;
    }

    return false;
}

/**
 * Helper function to identify if a hand of cards is consider a Straight in 
 * Poker
 * 
 * Formula:
 * Case 1 -> A 2 3 4 5 [integer representation: 0 1 2 3 4]
 * We are checking that if the hand starts with an Ace, then the other values 
 * are in consecutive order
 * 
 * Case 2 -> A T J Q K [integer representation: 0 9 10 11 12]
 * We are checking that if the hand starts with an Ace, then the other values
 * are: T J Q K
 * 
 * Case 3 -> Any combination of ranks in consecutive order
 * 
 * @property playerHand The hand of cards that is going to be checked
 * 
 * @return Whether the hand of cards is consider a Straight or not in Poker
 */
bool IsStraight(struct Hand playerHand)
{
    if(playerHand.Cards[0].Rank == 0)
    {
        if(
            playerHand.Cards[1].Rank == 1 && playerHand.Cards[2].Rank == 2 &&
            playerHand.Cards[3].Rank == 3 && playerHand.Cards[4].Rank == 4 
        )
        {
            return true;
        }

        if(
            playerHand.Cards[1].Rank == 9 && playerHand.Cards[2].Rank == 10 &&
            playerHand.Cards[3].Rank == 11 && playerHand.Cards[4].Rank == 12 
        )
        {
            return true;
        }

        return false;
    }
    else
    {
        int previousCard = 0;
        int previousRank = 0;
        int currentCard = 1;
        int currentRank = 0;
        bool inConsecutiveOrder = true;

        while(inConsecutiveOrder == true && currentCard < AMOUNT_OF_CARDS_PER_PLAYER)
        {
            previousCard = currentCard - 1;
            previousRank = playerHand.Cards[previousCard].Rank;
            currentRank = playerHand.Cards[currentCard].Rank;

            if( (currentRank - 1) != previousRank)
            {
                inConsecutiveOrder = false;
            }

            currentCard++;
        }

        return inConsecutiveOrder;
    }
}

/**
 * Helper function to identify if a hand of cards is consider Three of a Kind in 
 * Poker
 * 
 * Formula:
 * Case 1 -> x x x y z
 * We are checking that the first 3 cards have the same rank
 * 
 * Case 2 -> y x x x z
 * We are checking that the 2nd, 3th, and 4th card have the same rank
 * 
 * Case 3 -> y z x x x
 * We are checking that the last 3 cards have the same rank
 * 
 * @property playerHand The hand of cards that is going to be checked
 * 
 * @return Whether the hand of cards is consider Three of a Kind or not in Poker
 */
bool IsThreeOfAKind(struct Hand playerHand)
{
    if(playerHand.Cards[0].Rank == playerHand.Cards[2].Rank)
    {
        return true;
    }

    if(playerHand.Cards[1].Rank == playerHand.Cards[3].Rank)
    {
        return true;
    }

    if(playerHand.Cards[2].Rank == playerHand.Cards[4].Rank)
    {
        return true;
    }

    return false;
}

/**
 * Helper function to identify if a hand of cards is consider Two Pairs in Poker
 * 
 * Formula:
 * Case 1 -> x x y y z
 * We are checking that the first 2 cards have the same rank, and that the 3rd
 * and 4th card have the same rank
 * 
 * Case 2 -> x x z y y
 * We are checking that the first 2 cards have the same rank, and that the last
 * 2 cards have the same rank
 * 
 * Case 3 -> z x x y y
 * We are checking that the 2nd and 3rd card have the same rank, and that the
 * last 2 cards have the same rank
 * 
 * @property playerHand The hand of cards that is going to be checked
 * 
 * @return Whether the hand of cards is consider Two Pairs or not in Poker
 */
bool IsTwoPairs(struct Hand playerHand)
{
    if(
        playerHand.Cards[0].Rank == playerHand.Cards[1].Rank &&
        playerHand.Cards[2].Rank == playerHand.Cards[3].Rank
    )
    {
        return true;
    }

    if(
        playerHand.Cards[0].Rank == playerHand.Cards[1].Rank &&
        playerHand.Cards[3].Rank == playerHand.Cards[4].Rank
    )
    {
        return true;
    }

    if(
        playerHand.Cards[1].Rank == playerHand.Cards[2].Rank &&
        playerHand.Cards[3].Rank == playerHand.Cards[4].Rank
    )
    {
        return true;
    }

    return false;
}