/*=============================================================================
|   Source code:  CommonFunctionalities.h
|        Author:  Alejandro Perez
|    Student ID:  6088462
|    Assignment:  Program #4 Assignment 4
|*===========================================================================*/

#ifndef CommonFunctionalities_H

/*=============================================================================
| INCLUDES ====================================================================
|*===========================================================================*/
/*
 * <stdio.h> It is being used in order to output information to the end user
 * <stdio.h> It is being used to convert string to integer, and to be able to
 *           generate random numbers
 * <time.h> It is used to get the current time
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*=============================================================================
| CONSTANTS ===================================================================
|*===========================================================================*/

/**
 * Helper define to avoid including this header file twice in the program
 */
#define CommonFunctionalities_H

#define AMOUNT_OF_CARDS_PER_PLAYER 5 // The amount of cards per player when
                                     // playing poker
#define CARD_SUITS 4 // The amount of unique suits that exists in a card deck 
#define CARD_RANKS 13 // The amount of card ranks
#define MIN_INPUT_VALUE 1 // The minimum value allowed for the amount of players
                          // and the amount of cards per player
#define MAX_INPUT_VALUE 10 // The maximum value allowed for the amount of 
                           //players and the amount of cards per player
#define CARD_SUIT_POSITION 0 // The position of the argument that contains the
                             // card suit in the deck array
#define CARD_RANK_POSITION 1 // The position of the argument that contains the
                             // card rank in the deck array
#define CARD_ARRAY_SIZE 2 // The size of the array that each card must have in
                          // order to hold the suit and the rank
#define POKER_HAND_HIGH_CARD 0 // Integer representation of a high card hand
#define POKER_HAND_ONE_PAIR 1 // Integer representation of a one pair hand
#define POKER_HAND_TWO_PAIRS 2 // Integer representation of a two pairs hand
#define POKER_HAND_THREE_OF_A_KIND 3 // Integer representation of a three of a
                                    // kind hand
#define POKER_HAND_STRAIGHT 4 // Integer representation of a straight hand
#define POKER_HAND_FLUSH 5 // Integer representation of a flush hand
#define POKER_HAND_FULL_HOUSE 6 // Integer representation of a full house hand
#define POKER_HAND_FOUR_OF_A_KIND 7 // Integer representation of a four of a
                                    // kind hand
#define POKER_HAND_STRAIGHT_FLUSH 8 // Integer representation of a straight 
                                    // flush hand

#define CONSOLE_COLOR_GREEN "\x1b[32m" // The value used to print green font
                                      // color to the console
#define CONSOLE_COLOR_RESET "\x1b[0m" // The value used to reset the console's
                                      // font color
#define CONSOLE_COLOR_RED "\x1B[31m" // The value used to print red font color
                                     // to the console
#define CONSOLE_COLOR_YELLOW "\x1B[33m" // The value used to print yellow font 
                                        // color to the console

#define BASE_TEN 10 // The base ten numbers representation
#define ERROR 0
#define INPUT_AMOUNT_OF_CARDS_POSITION 1 // The position of the argument that
                                        // contains the amount of cards per
                                        // player in the console's arguments
                                        // array
#define INPUT_AMOUNT_OF_PLAYERS_POSITION 2 // The position of the argument that
                                          // contains the amount of players in 
                                          // the console's arguments array
#define MIN_INPUT_VALID_ARGUMENTS_COUNT 3 // The minimum amount of arguments
                                          // required for the program to work
#define SUCCESS 0

/*=============================================================================
| CUSTOM TYPES ================================================================
|*===========================================================================*/

/**
 * Replicate the Boolean functionality from other programming languages
 */
typedef enum { 
    false, 
    true 
} bool;

/**
 * Struct to represent a card
 * 
 * @property Rank The rank of the card
 * @property Suit The suit of the card
 */
struct Card {
    int Rank;
    int Suit;
};

/**
 * Struct to represent a player's hand
 * 
 * @property Cards The cards of the player
 * @property Winner Whether this hand is a winner hand
 * @property HandName The integer representation of the hand name under the 
 *                    poker terminology
 */
struct Hand {
    struct Card Cards[AMOUNT_OF_CARDS_PER_PLAYER];
    bool Winner;
    int Category;
};

/*=============================================================================
| FUNCTIONS ===================================================================
|*===========================================================================*/

int AmountOfCardRequiredForPlayers(int, int);
void DealCardsToPlayers(
    const struct Card[],
    struct Hand[],
    int,
    int
);
int GetAmountOfCards();
char GetCardRankCharacter(int);
char* GetHandName(int);
int GetRandomPositionInDeck();
struct Hand GetTestHandFlush();
struct Hand GetTestHandFourOfAKind();
struct Hand GetTestHandFullHouse();
struct Hand GetTestHandHighCard();
struct Hand GetTestHandOnePair();
struct Hand GetTestHandStraight();
struct Hand GetTestHandStraightFlush();
struct Hand GetTestHandThreeOfAKind();
struct Hand GetTestHandTwoPairs();
int InitializeCardRank(int);
int InitializeCardSuit(int);
void InitializeDeck(struct Card[]);
bool IsFlush(struct Hand);
bool IsFourOfAKind(struct Hand);
bool IsFullHouse(struct Hand);
bool IsStraight(struct Hand);
bool IsThreeOfAKind(struct Hand);
bool IsTwoPairs(struct Hand);
bool IsOnePair(struct Hand);
void OrderPlayerHand(struct Hand*);
void PokerGame(const int, const int);
void PrintCard(struct Card);
void PrintDeck(const struct Card[]);
void PrintInput(int, int);
void PrintPlayersDeck(
    const struct Hand[],
    int,
    int,
    bool,
    bool
);
void PrintProgramHeader();
void PrintTestHands();
void PrintTextDanger(char[]);
void PrintTextSuccess(char[]);
void PrintTextWarning(char[]);
void PrintTextWithColor(char[], char[]);
void RankPlayerHand(struct Hand*);
void RankPlayersHands(struct Hand[], int);
void ShuffleDeck(struct Card[]);
void SwapCardsPositionInDeck(struct Card[], int, int);
bool ValidateAmountOfCards(int);
bool ValidateAmountOfPlayers(int);
bool ValidateConsoleArgumentsCount(int);
bool ValidateConsoleValues(int, char**, int*, int*);
bool ValidateEnoughCardsToPlay(int, int);

#endif